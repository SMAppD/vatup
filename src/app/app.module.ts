import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { FeedPage } from '../pages/feed/feed';
import { CommentsPage } from '../pages/comments/comments';

import { Camera } from '@ionic-native/camera';
import { Firebase } from '@ionic-native/firebase';

import firebase from 'firebase';
import { DatabaseProvider } from '../providers/database/database';
import { SocialFeedPage } from '../pages/social-feed/feed';
import { HttpModule } from '@angular/http';
import { MediaPlayerPage } from '../pages/media-player/media-player';
import { MediaCapture } from '@ionic-native/media-capture';
import { IonicStorageModule } from '@ionic/storage';
import { File } from '@ionic-native/file';
import { Media } from '@ionic-native/media';

var firebaseConfig = {
  apiKey: "AIzaSyDi6xq2ctt43EJJ4m3Z99Cj6ldLDfJROiQ",
  authDomain: "feedly-c782f.firebaseapp.com",
  databaseURL: "https://feedly-c782f.firebaseio.com",
  projectId: "feedly-c782f",
  storageBucket: "feedly-c782f.appspot.com",
  messagingSenderId: "1076834636983"
};
firebase.initializeApp(firebaseConfig);
firebase.firestore().settings({
  timestampsInSnapshots: true
})

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    SignupPage,
    FeedPage,
    CommentsPage,
    MediaPlayerPage,
    SocialFeedPage
  ],
  
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    SignupPage,
    FeedPage,
    CommentsPage,
    SocialFeedPage,
    MediaPlayerPage,
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    Firebase,
    MediaCapture,
    File,
    Media,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseProvider
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
