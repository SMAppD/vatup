import { NgModule } from '@angular/core';
import { SocialFeedPage } from './feed';
import { IonicPageModule } from 'ionic-angular';
import { ShrinkHeaderModule } from '../../components/shrink-header/shrink-header.module';
@NgModule({
  declarations: [
    SocialFeedPage
  ],
  imports: [
    IonicPageModule.forChild(SocialFeedPage),
    ShrinkHeaderModule 
  ],
  exports: [
    SocialFeedPage
  ]
})
export class FeedPageModule {}
